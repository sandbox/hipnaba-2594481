<?php
/**
 * @file
 * Hooks provided by the Bower Manager module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Allow modules to alter the consolidated JSON array.
 *
 * @param array &$json
 *   The consolidated JSON compiled from each module's bower.json file.
 */
function hook_bower_json_alter(&$json) {
  $json['private'] = true;
}