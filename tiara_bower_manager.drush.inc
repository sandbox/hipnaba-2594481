<?php
/**
 * @file
 *
 * Provides a Drush frontend for Bower Manager.
 */

/**
 * Implements hook_drush_command().
 *
 * @see hook_drush_command()
 */
function tiara_bower_manager_drush_command() {
  $commands = array();

  $commands['tiara-bower-manager'] = array(
    'description' => 'Executes `bower install` in the directory containing bower.json.',
    'aliases' => array('tbm'),
  );

  $commands['tiara-bower-json-rebuild'] = array(
    'description' => 'Rebuilds the consolidated bower.json file.',
    'aliases' => array('tbjr'),
  );

  return $commands;
}

/**
 * Executes `bower install` in the directory containing bower.json.
 */
function drush_tiara_bower_manager() {
  $uri = variable_get('tiara_bower_manager_file_dir', file_default_scheme() . '://bower');
  if (!$dir = drupal_realpath($uri)) {
    return drush_set_error(dt('Error resolving path: @uri', array('@uri' => $uri)));
  }

  // Check to see if the bower.json file is available.
  if (!file_exists($dir . '/bower.json')) {
    if (drush_confirm(dt('The bower.json file is missing. Would you like to re-build it?'))) {
      if (drush_invoke('tiara-bower-json-rebuild') === FALSE) {
        return FALSE;
      }
    }
    else {
      return drush_set_error(dt('Missing bower.json file: Run `drush tiara-bower-json-rebuild`.'));
    }
  }

  // Check if Drush Bower is available.
  if (!drush_is_command('tiara-bower')) {
    // Ask if the user would like to install Drush Bower.
    if (!drush_confirm(dt('Download and install the Drush Bower extension?'))) {
      drush_print(dt('Installation of the Drush Bower extension was aborted.'));
      return TRUE;
    }

    // Download the Drush extension.
    if (drush_invoke('dl', array('tiara_bower-7.x-1.x')) === FALSE) {
      return drush_set_error(dt('Failed installing the Drush Bower extension. Install it manually by following the instructions on the project page: https://www.drupal.org/sandbox/hipnaba/2592355'));
    }

    // Clear the caches by reloading the available command files.
    _drush_find_commandfiles(DRUSH_BOOTSTRAP_NONE);
  }

  // Bower arguments.
  $bower_arguments = array(
    '--working-dir=' . $dir,
    'install'
  );

  // Run Bower
  return drush_invoke('tiara-bower', $bower_arguments);
}

/**
 * Writes the master bower.json file.
 *
 * @return bool
 */
function drush_tiara_bower_manager_tiara_bower_json_rebuild() {
  // Ensure only one process runs at a time.
  if (!lock_acquire(__FUNCTION__, 10)) {
    return FALSE;
  }

  try {
    // Collect the data from modules.
    $data = array();
    foreach (module_list() as $module) {
      $module_dir = drupal_get_path('module', $module);
      $bower_file = $module_dir . '/bower.json';
      $args = array('@file' => $bower_file);

      if (!file_exists($bower_file)) {
        continue;
      }

      if (!$filedata = @file_get_contents($bower_file)) {
        if (!$filedata = @file_get_contents($bower_file)) {
          throw new \RuntimeException(t('Error reading file: @file', $args));
        }
      }

      if (!$bower_json = @drupal_json_decode($filedata)) {
        throw new \UnexpectedValueException(t('Expecting contents of file to be valid JSON: @file', $args));
      }

      $data[$module] = $bower_json;
    }

    // Generate the JSON for the file.
    $bower_json = array(
      'name' => variable_get('site_name', ''),
      'dependencies' => array(),
    );

    foreach ($data as $module => $json) {
      if (isset($json['dependencies'])) {
        $bower_json['dependencies'] = array_merge($bower_json['dependencies'], $json['dependencies']);
      }
    }

    drupal_alter('bower_json', $combined);

    // Write the file.

    // Make the composer.json file human readable for PHP >= 5.4.
    // @see drupal_json_encode()
    // @see http://drupal.org/node/1943608
    // @see http://drupal.org/node/1948012
    $json_options = JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT;
    if (defined('JSON_PRETTY_PRINT')) {
      $json_options = $json_options | JSON_PRETTY_PRINT;
    }
    if (defined('JSON_UNESCAPED_SLASHES')) {
      $json_options = $json_options | JSON_UNESCAPED_SLASHES;
    }

    $dir_uri = variable_get('tiara_bower_manager_file_dir', file_default_scheme() . '://bower');

    if (!is_dir($dir_uri)) {
      drupal_mkdir($dir_uri, NULL, TRUE);
    }

    $file_uri = $dir_uri . '/bower.json';
    if (!@file_put_contents($file_uri, json_encode($bower_json, $json_options))) {
      throw new \RuntimeException(t('Error writing bower.json file: @file', array('@file' => $file_uri)));
    }

    // Displays a message to admins that a file was written.
    if (user_access('administer site configuration')) {
      drupal_set_message(t('A bower.json file was written to @path.', array('@path' => drupal_realpath($dir_uri))));
      if ('admin/config/system/tiara-bower-manager' != current_path()) {
        $args = array('@url' => url('https://www.drupal.org/sandbox/hipnaba/2594481', array('absolute' => TRUE)));
        $message = t('Bower\'s <code>install</code> command must be run to install the required packages.<br/>Refer to the instructions on the <a href="@url" target="_blank">Tiara Bower Manager project page</a> for installing packages.', $args);
        drupal_set_message($message, 'warning');
      }
    }

    // Ensure that .bowerrc points to the correct directory
    $bowerrc_uri = $dir_uri . '/.bowerrc';
    $relative_path = _tiara_bower_manager_relative_dir(realpath('sites/all/libraries'), drupal_realpath($dir_uri));

    $bowerrc_json = array(
      'directory' => $relative_path
    );

    if (!@file_put_contents($bowerrc_uri, json_encode($bowerrc_json, $json_options))) {
      throw new \RuntimeException(t('Error writing bower.json file: @file', array('@file' => $file_uri)));
    }

    $success = TRUE;
  }
  catch (RuntimeException $e) {
    if (user_access('administer site configuration')) {
      drupal_set_message(t('Error writing bower.json file'), 'error');
    }
    watchdog_exception('tiara_bower_manager', $e);
    $success = FALSE;
  }

  lock_release(__FUNCTION__);
  return $success;
}

/**
 * Gets the path of the "to" directory relative to the "from" directory.
 *
 * Stolen from Composer Manager
 *
 * @param array $dir_to
 *   The absolute path of the directory that the relative path refers to.
 * @param array $dir_from
 *   The absolute path of the directory from which the relative path is being
 *   calculated.
 *
 * @return string
 */
function _tiara_bower_manager_relative_dir($dir_to, $dir_from) {
  $dirs_to = explode('/', ltrim($dir_to, '/'));
  $dirs_from = explode('/', ltrim($dir_from, '/'));

  // Strip the matching directories so that both arrays are relative to a common
  // position. The count of the $dirs_from array tells us how many levels up we
  // need to traverse from the directory containing the composer.json file, and
  // $dirs_to is relative to the common position.
  foreach ($dirs_to as $pos => $dir) {
    if (!isset($dirs_from[$pos]) || $dirs_to[$pos] != $dirs_from[$pos]) {
      break;
    }
    unset($dirs_to[$pos], $dirs_from[$pos]);
  }

  $path = str_repeat('../', count($dirs_from)) . join('/', $dirs_to);
  if (PHP_OS == 'WINNT'){
    $path = preg_replace('%..\\/([a-zA-Z])%i', '${1}', $path, 1);
  }
  return $path;
}